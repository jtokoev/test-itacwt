Щаги для запуска проекта
1. git clone git@github.com:jtokoev/test-one-payment.git
2. cd test-one-payment/
3. Cборка контейнеров

```make build```

4. Загрузка стуктуры и данных в базу

```make build_db```

5. Тесты

```make test```

6. Проверяем результат

  http://localhost:1234/find_range


7. Во вопросам прошу обращаться в телегу @tokoevj