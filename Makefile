PHP_CONTAINER := test_php

build:
	cp .env.dist .env
	cp app/.env.dist app/.env
	cp docker-compose.override.yml.dist docker-compose.override.yml
	docker-compose down && docker-compose up -d --build
	docker exec ${PHP_CONTAINER} composer install

build_db:
	docker exec ${PHP_CONTAINER} bin/console d:m:m
	docker exec ${PHP_CONTAINER} bin/console app:push-data

test:
	docker exec ${PHP_CONTAINER} bin/phpunit
